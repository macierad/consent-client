package com.idemia;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@SpringBootApplication
public class DemoApplication {

	RestTemplate restTemplate;
	String addonPath;

	public DemoApplication(RestTemplateBuilder restTemplateBuilder, @Value("${addpath}") String addonPath) {
		this.restTemplate = restTemplateBuilder.build();
		this.addonPath = addonPath;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("/test")
	public void test() {
		String body = "{\n" +
				"\t\"ppId\" : \"1234567\",\n" +
				"\t\"consentId\" : \"12345678\",\n" +
				"\t\"callbackUrl\" : \"http://aaaa.wp\",\n" +
				"\t\"message\" : {\n" +
				"\t\t\"header\":\"asd\",\n" +
				"\t\t\"subject\":\"asd\",\n" +
				"\t\t\"body\":\"ads\",\n" +
				"\t\t\"loa\":1\n" +
				"\t}\n" +
				"}";
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Correlation-ID", "test-header-value");
		headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		restTemplate.postForEntity( addonPath + "/api/v1/consent", entity, Void.class);
	}

}
